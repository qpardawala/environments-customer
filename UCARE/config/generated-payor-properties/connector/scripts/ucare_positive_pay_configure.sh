#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./ucare_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** UCARE SH : ucare_positive_pay_configure.sh start on ${HOST_NAME} **********"


    echo "Start configuring positive pay job"

        positivepayJsonConfigFile=$HE_DIR/ucare-positivepay/resources/config/positivepay-job-config.json
        positivepayJsonRouteFile=$HE_DIR/ucare-positivepay/resources/config/positivepay-route-config.json

        chmod -R 755 $positivepayJsonConfigFile
        chmod -R 755 $positivepayJsonRouteFile

        sed -i -e "s|#HE_DIR#|${HE_DIR}|g" $positivepayJsonConfigFile

          extractEndTime=$(date +"%Y-%m-%dT%T.%3NEDT")
        extractStartEndTime=$(date +"%Y-%m-%dT%T.%3NEDT" -d "1 month ago")
        echo "extractStartEndTime: ${extractStartEndTime}"
        echo "extractEndTime: ${extractEndTime}"

        sed -i -e "s|SSSS-MM-DDTHH:MM:SS.SSSZEDT|${extractStartEndTime}|g" $positivepayJsonConfigFile
        sed -i -e "s|EEEE-MM-DDTHH:MM:SS.SSSZEDT|${extractEndTime}|g" $positivepayJsonConfigFile

 	bankInfoInputDir=$(grep "bankInfoInputDir" $HE_DIR/etc/com.healthedge.customer.ucare.nacha.positive.pay.extract.cfg | sed -e "s/bankInfoInputDir=//g")
        cp $HE_DIR/etc/BANK*.cfg ${bankInfoInputDir}/

        extractOutputPath=$(grep "extractFolder" $HE_DIR/etc/com.healthedge.customer.ucare.nacha.positive.pay.extract.cfg | sed -e "s/extractFolder=//g")
        echo "extractOutputPath: ${extractOutputPath}"

        sed -i -e "s|#EXTRACT_OUTPUT_PATH#|${extractOutputPath}|g" $positivepayJsonConfigFile

        extractConfigRequestDir=$(grep "extractConfigRequestDir" $HE_DIR/etc/com.healthedge.he.common.extract.configuration.cfg | sed -e "s/extractConfigRequestDir=//g")
        echo "extractConfigRequestDir: ${extractConfigRequestDir}"

        echo "Dropping positive pay job configuration json ${positivepayJsonConfigFile} to ${extractConfigRequestDir}"
        cp $positivepayJsonConfigFile $extractConfigRequestDir

    echo "End configuring positive pay job"


    echo "Start configuring positive pay job trigger route"

        extractCron=$(getProperty positive_pay_extract_job_trigger_cron)
        echo "extractCron: ${extractCron}"

        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $positivepayJsonRouteFile

        fileRequestBaseDir=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        stagingDir=$(grep "stagingDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/stagingDir=//g")
        triggerFileBaseDir=$(grep "triggerFileBaseDir" $HE_DIR/etc/com.healthedge.he.common.scheduler.cfg | sed -e "s/triggerFileBaseDir=//g")

	mkdir -p ${fileRequestBaseDir}
	mkdir -p ${stagingDir}
	mkdir -p ${triggerFileBaseDir}

        fileRequestBaseDir=${fileRequestBaseDir}request/cron

        echo "Dropping positive pay job trigger creation json ${positivepayJsonRouteFile} to ${fileRequestBaseDir}"
        cp $positivepayJsonRouteFile $fileRequestBaseDir

    echo "End configuring positive pay job trigger route"


echo "********** UCARE SH : ucare_positive_pay_configure.sh end on ${HOST_NAME} **********"
echo ""
