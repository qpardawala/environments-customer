#!/bin/bash
set -e

HE_DIR=$1
HOST_NAME=$2

PROPERTY_FILE=./bcbsne_env.properties

function getProperty {
   PROP_KEY=$1
   PROP_VALUE=`
   cat $PROPERTY_FILE | grep "$PROP_KEY" | cut -d'=' -f2`
   echo $PROP_VALUE
}

echo ""
echo "********** BCBSNE SH : bcbsne_nacha_sub_configure.sh start on ${HOST_NAME} **********"


    echo "Start configuring nacha_sub trigger route"

        nachaSubJsonRouteFile=$HE_DIR/bcbsne-nachasub/resources/config/nachasub-route-config.json

        extractCron=$(getProperty nacha_sub_extract_job_frequency)
        echo "extractCron: ${extractCron}"

        sed -i -e "s|#EXTRACT_TIME#|${extractCron}|g" $nachaSubJsonRouteFile

        schedulerStagingDir=$(grep "stagingDir" $HE_DIR/etc/com.healthedge.customer.generic.scheduler.cfg | sed -e "s/stagingDir=//g")
        mkdir -p $schedulerStagingDir

        fileRequestBaseDir=$(grep "fileRequestBaseDir" $HE_DIR/etc/com.healthedge.customer.generic.scheduler.cfg | sed -e "s/fileRequestBaseDir=//g")
        fileRequestBaseDir=${fileRequestBaseDir}/request/cron

        mkdir -p $fileRequestBaseDir

        echo "Dropping nacha_sub job trigger creation json ${nachaSubJsonRouteFile} to ${fileRequestBaseDir}"
        cp $nachaSubJsonRouteFile $fileRequestBaseDir/nachasub-route-config.json

    echo "End configuring nacha_sub job trigger route"


echo "********** BCBSNE SH : bcbsne_nacha_sub_configure.sh end on ${HOST_NAME} **********"
echo ""
